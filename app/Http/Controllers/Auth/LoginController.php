<?php

namespace App\Http\Controllers\Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Profile;
use Auth;


class LoginController extends Controller
{
    public function login()
    {

       $credentials= $this->validate(request(),[

            'email'=>'email|required|string',
            'password'=>'required|string'

        ]);

       
        if(Auth::attempt( $credentials))
        {
            $jobsall=Job::all();
            $profilesall=Profile::all(); 
            return view('welcome',compact('profilesall','jobsall') );
        }
      
        return back()
        ->withErrors(['email'=>trans('auth.failed')])->withInput(request(['email']));

    }  

    public function logout()
    {

      Auth::logout();
      return redirect('/');

    } 






}
