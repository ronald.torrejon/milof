<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Job;
use App\Profile;
Use Carbon\Carbon;


class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $jobsall=Job::all();
        $profilesall=Profile::all();
        $profiles=Profile::paginate(9);
        return view('profiles.index',compact('profiles', 'profilesall','jobsall') );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($request->hasFile('photo')) 
        {
           $file =$request->file('photo');
           $photo = time().$file->getClientOriginalName();
           $file->move(public_path().'/images/',$photo);
        }

        $profile= new Profile();
        
        $profile->job_id=$request->input('job_id');
        $profile->resume=$request->input('resume');
        $profile->photo=$photo;
        $profile->save();
        return 'Saved';
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        $jobsall=Job::all();
        $profilesall=Profile::all();
        $profile=Profile::where('slug','=',$slug)->firstOrFail();
        
        return view('profiles.show',compact('profile','profilesall' ,'jobsall') );
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
