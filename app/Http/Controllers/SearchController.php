<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Job;


class SearchController extends Controller
{
    public function searchWelcome(){
      
      $jobsall=Job::all();

		  return view('welcome',compact('jobsall'));

   }

   public function searchNosotros(){

    	$jobsall=Job::all();
		  return view('nosotros.nosotros',compact('jobsall'));

   }

   public function searchPlanes(){

    	$jobsall=Job::all();
		  return view('plans.planes',compact('jobsall'));

   }

   public function searchMilof(){

    	$jobsall=Job::all(); 
		  return view('milof.milof',compact('jobsall'));

   }
}
