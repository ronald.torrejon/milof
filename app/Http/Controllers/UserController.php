<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Profile;
Use Carbon\Carbon;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()

    {
        $profilesall=Profile::all();     
        $users=User::paginate(9);
        return view('users.index',compact('users', 'profilesall') );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('users.registro');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user= new User();
        
        $user->names=$request->input('names');
        $user->surnames=$request->input('surnames');
        $user->type_doc=$request->input('type_doc');
        $user->num_doc=$request->input('num_doc');
        $user->birth=$request->input('birth');
        $user->country=$request->input('country');
        $user->div_1=$request->input('div_1');
        $user->location=$request->input('location');
        $user->address=$request->input('address');
        $user->mobile=$request->input('mobile');
        $user->sex=$request->input('sex');
        $user->referred=$request->input('referred');
        $user->points_ref=0;
        $user->plan='free';
        $user->courses=0;
        $user->locked=0;
        $user->slug=$request->input('surnames').$request->input('names');

        //$user->password=$request->input('password');
        $user->password = bcrypt($request->password);

//$encrypted = encrypt($unencrypted_value);
//$password = bcrypt('my-secret-password');


        $user->email=$request->input('email');
        $user->save();
        return 'Saved';
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        $profilesall=Profile::all();  //$profile::all ; hace refencia al modelo de la table llamado Profile
        $user=User::where('slug','=',$slug)->firstOrFail();
        //$asociado=User::find($id);
        return view('users.show',compact('user', 'profilesall') );
        //return $slug;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
