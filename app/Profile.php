<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
//use App\

class Profile extends Model
{
   
   

	public function getPosterUsername()
	{

		return User::where('id', $this->user_id)->first()->names;
	}
	public function getPosterUsersurname()
	{

		return User::where('id', $this->user_id)->first()->surnames;
	}

	public function getPosterUsermobile()
	{

		return User::where('id', $this->user_id)->first()->mobile;
	}

	public function getPosterUsertype_doc()
	{

		return User::where('id', $this->user_id)->first()->type_doc;
	}

	public function getPosterUsernum_doc()
	{

		return User::where('id', $this->user_id)->first()->num_doc;
	}

	public function getPosterUserbirth()
	{

		return User::where('id', $this->user_id)->first()->birth;
	}

	public function getPosterUseremail()
	{

		return User::where('id', $this->user_id)->first()->email;
	}

	public function getPosterJobname()
	{
		return Job::where('id', $this->user_id)->first()->name;
	}



}
