<?php

use Faker\Generator as Faker;

$factory->define(App\Account::class, function (Faker $faker) {
     $title = $faker->sentence(6);
    return [
        
        'slug'    			=> str_slug($title),
        'user_id'    		=> rand(1,30),
        'count'				=> $faker->text(15),
        'earnings'  		=> $faker->numberBetween(000,9999),
        'last_payment'  	=> $faker->numberBetween(000,199),
        'total_payments'  	=> $faker->numberBetween(000,9999),
        'description'		=> $faker->text(250),

    ];
});
