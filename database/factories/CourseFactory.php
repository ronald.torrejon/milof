<?php

use Faker\Generator as Faker;

$factory->define(App\Course::class, function (Faker $faker) {
    $title = $faker->sentence(6);
    return [
        
        'slug'    		=> str_slug($title),
        'name'    		=>  $title,
        'trainer'		=>  $faker->text(15),
        'code'  		=> $faker->text(5),
        'description'	=>  $faker->text(250),

    ];
});
