<?php

use Faker\Generator as Faker;

$factory->define(App\Job::class, function (Faker $faker) {
    $title = $faker->sentence(6);
    return [
        
        'slug'    		=> str_slug($title),
        'name'    		=> $faker->jobTitle,
        'area'  		=> $faker->text(10),
        'description' 	=> $faker->text(250),

    ];
});
