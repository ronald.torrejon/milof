<?php

use Faker\Generator as Faker;

$factory->define(App\Locked::class, function (Faker $faker) {
    $title = $faker->sentence(6);
    return [
        
		'user_id' => rand(1,30),
        'slug'    => str_slug($title),
        'reason'  => $faker->text(250),

    ];
});
