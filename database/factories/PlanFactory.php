<?php

use Faker\Generator as Faker;

$factory->define(App\Plan::class, function (Faker $faker) {
    $title = $faker->sentence(6);
    return [
        
		'name'    		=>  $title,
        'slug'    		=> str_slug($title),
        'cost'   		=> $faker->numberBetween(0,135),
        'description'  	=> $faker->text(250),

    ];
});
