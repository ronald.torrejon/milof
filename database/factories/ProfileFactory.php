<?php

use Faker\Generator as Faker;

$factory->define(App\Profile::class, function (Faker $faker) {
    
    $title = $faker->sentence(6);
    return [
        
		'user_id' => rand(1,30),
		'job_id' => rand(1,50),
        'slug'    => str_slug($title),
        'photo'   => $faker->imageUrl($width=300, $height=300),
        'resume'  => $faker->text(250),
        'qualification'  => $faker->randomfloat(1,0.5,5),
        'services' => rand(1,50),

    ];
});
