<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\User::class, function (Faker $faker) {
    
    $title = $faker->name;
    return [
     
        'names'     => $faker->firstName,
        'slug'      => str_slug($title),
        'surnames'  => $faker->lastName,
        'type_doc'  => $faker->randomElement(['CC', 'DNI','PASAPORTE', 'CE', 'CI']),
        'num_doc'   => $faker->numberBetween(900000000,1129999999),
        'birth'     => $faker->date,
        'country'   => $faker->randomElement(['CHILE', 'COLOMBIA','PERÚ']),
        'div_1'     => $faker->randomElement(['Amazonas', 'Santiago','Piura','Lima','Cundinamrca']),
        'location'  => $faker->randomElement(['Amazonas', 'Santiago','Piura','Lima','Bogotá']),
        'address'   => $faker->randomElement(['Av Grau 1399', 'Cra 12 #3936','Santo Domingo 102']),
        'mobile'    => $faker->PhoneNumber,
        'sex'       => $faker->randomElement(['Hombre', 'Mujer','Otro',]),
        'referred'  => $faker->numberBetween(1,999),
        'points_ref' => rand(1,50),
        'plan'      => $faker->randomElement(['MILOF FREE', 'MILOFX2','MILOX4', 'MILOFX6', 'MILOFX12']),
        'courses'   => $faker->randomElement(['0', '1']),
        'locked'    => $faker->randomElement(['0', '1']),


        'email' => $faker->unique()->safeEmail,
        'email_verified_at' => now(),
        'password' => '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', // secret
        'remember_token' => str_random(10),
    ];
});
