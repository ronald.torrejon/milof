<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {


            $table->increments('id');
            $table->string    ('slug')->unique();
            $table->string    ('names');
            $table->string    ('surnames');
            $table->string    ('type_doc');
            $table->string    ('num_doc');
            $table->date      ('birth');
            $table->string    ('country');
            $table->string    ('div_1');     
            $table->string    ('location');
            $table->string    ('address');
            $table->string    ('mobile');     
            $table->string    ('sex');
            $table->string    ('referred');
            $table->string    ('points_ref');
            $table->string    ('plan');
            $table->boolean   ('courses');
            $table->boolean   ('locked');    
            $table->string    ('email')->unique();
            $table->timestamp ('email_verified_at')->nullable();
            $table->string    ('password');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
