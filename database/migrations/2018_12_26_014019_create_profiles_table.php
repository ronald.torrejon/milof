<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profiles', function (Blueprint $table) {
            $table->increments('id');
            $table->integer   ('user_id')->unsigned();
            $table->integer   ('job_id')->unsigned();
            $table->string    ('slug')->unique();
            $table->string    ('photo')->nullable();
            $table->mediumtext('resume');
            $table->float('qualification');
            $table->float('services');

            $table->timestamps();
         

            $table->foreign('user_id')->references('id')->on('users')
                  ->onDelete('cascade')
                  ->onUpdate('cascade');  
            $table->foreign('job_id')->references('id')->on('jobs')
                  ->onDelete('cascade')
                  ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('profiles');
    }
}
