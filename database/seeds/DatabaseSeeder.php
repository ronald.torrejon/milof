<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        $this->call(ProfilesTableSeeder::class);
        $this->call(PlansTableSeeder::class);
        $this->call(JobsTableSeeder::class);
        $this->call(LockedsTableSeeder::class);
        $this->call(CoursesTableSeeder::class);
        $this->call(AccountsTableSeeder::class);
    }
}
