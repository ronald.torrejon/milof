<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\User::class,29)->create();
        App\User::create([
        	'names'	 	=> 'Ronal Paul',
        	'slug'		=> 'Ronal Paul',
        	'surnames' 	=> 'Torrejón Infante',
        	'type_doc'	=> 'DNI',
        	'num_doc' 	=> '41153656',
        	'birth'     => '1979-09-21',
	        'country'   => 'PERÚ',
	        'div_1'     => 'Piura',
	        'location'  => 'Piura',
	        'address'   => 'Av Grau 1399',
	        'mobile'    => '923370737',
	        'sex'       => 'Hombre',
	        'referred'  => '0001',
	        'points_ref' => '15',
	        'plan'      => 'MILOFX2',
	        'courses'   => '0',
	        'locked'    => '0',
	        'email' => 'rptorrejoninfante@gmail.com',
	        'password' => bcrypt('123'),



        ]);
    }
}
