<!DOCTYPE html>
<html>
	<head>
		<title>1000ficios - @yield('title')</title>
		@yield('styles')
	</head>
	<body>
		@yield('content')
        @yield('scripts')
	</body>
</html>