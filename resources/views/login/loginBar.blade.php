<header>

    <div class="navbar navbar-dark bg-info shadow-sm">
        <div class="col-md-6 ">
            <a href="{{ url('/planes') }}" class="navbar-brand d-flex align-items-center">
                <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="mr-2"><path d="M23 19a2 2 0 0 1-2 2H3a2 2 0 0 1-2-2V8a2 2 0 0 1 2-2h4l2-3h6l2 3h4a2 2 0 0 1 2 2z"></path><circle cx="12" cy="13" r="4"></circle></svg>
                <strong>1000ficios</strong>
            </a>      
        </div>


		@if (Route::has('login'))
	       
	            @auth
	                <form class="col-md-6 "  method="POST"  action="{{route('logout')}}">
            			{{csrf_field()}}
            
		                <div class="row">
		                    <div class="col-sm-5">
		                      
		                          Bienvenido
		                    </div>
		                    <div class="col-sm-5">
		                          {{auth()->user()->names}}
		                    </div>

		                    <div class="col">
		                        <button class="btn btn-outline-dark">Salir</button>
		                    </div>
		                </div>

        			</form>

	    		@else
	        		
	    			<form class="col-md-6 "  method="POST"  action="{{route('login')}}" >
            			{{csrf_field()}}
            
	                	<div class="row ">
	                    	<div class="col-sm-5 {{$errors->has('email') ? 'has-error' : ''}} ">
	                      
	                      
	                          <input type="email" name="email" value="{{old('email')}}" class="form-control" placeholder="E-mail">
	                          {!! $errors->first ('email','<span class="help-block">:message</span>')!!}

	                    	</div>
                   			<div class="col-sm-5">
	                          <input type="password"  name="password"class="form-control" placeholder="Contraseña">
	                          	{!! $errors->first ('password','<span class="help-block">:message</span>')!!}
                          
                    		</div>

			                <div class="col">
			                    <button class="btn btn-outline-dark">Entrar</button>
			                </div>
			           
                		</div>
                		<a href="{{ url('/reg') }}" class="badge badge-light">Registrar</a>
        			</form>
	            @endauth
	        
	    @endif
</div>
</header>