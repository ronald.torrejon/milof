@extends('layouts.layout2')
@section('title', 'Entra o Registrate')


@section('styles')
    @include('common.head')
@endsection

@section('content')
    
    @include('login.loginbar')
    @include('profiles.search')


    <div class="pricing-header px-3 py-3 pt-md-5 pb-md-4 mx-auto text-center">
      <h1 class="display-4">MIRA NUESTROS PLANES</h1>
      <p class="lead">Tenemos las mejores opciones para ti.</p>
    </div>

    <div class="container">
      <div class="card-group mb-1 text-center">
        <div class="card mb-4 shadow-sm">
          <div class="card-header">
              <h4 class="my-0 font-weight-normal"> Milof Free</h4>
          </div>
          <div class="card-body">
            <h1 class="card-title pricing-card-title">$0 <small class="text-muted"><br> x 3 meses</small></h1>
            <ul class="list-unstyled mt-1 mb-2">
              <li>Una cuenta básica.</li>
              <li>Una categoría de oficio.</li>
              <li>Busque perfiles de otros usuarios.</li>
              <li><h4 class="btn-link btn-primary" data-toggle="modal" data-target="#exampleModal0">Más!</h4></li>
              <!-- Modal -->
              <div class="modal fade" id="exampleModal0" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title" id="exampleModalLabel">Milof Free</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body">
                      <ul class="list-group">
                        <li class="list-group-item">Una cuenta Básica es para cualquier persona que desee crear y mantener un perfil en línea.</li>
                        <li class="list-group-item">Cree su perfil laboral y empiece a hacer dinero en su tiempo libre</li>
                        <li class="list-group-item">Su perfil en una categoría de oficio.</li>
                        <li class="list-group-item">Amplíe su red de contactos laborales</li>
                        <li class="list-group-item">Proporcione recomendaciones calificando a los demás, según el servicio recibid</li>
                        <li class="list-group-item">Busque los perfiles de otros usuarios de MilOficios.</li>
                      </ul>
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                      <button type="button" class="btn btn-primary">Registrate Gratis</button>
                    </div>
                  </div>
                </div>
              </div>
            </ul>
            <button type="button" class="btn btn-lg btn-block btn-outline-primary">Registrate gratis</button>
          </div>
        </div>
        <div class="card mb-4 shadow-sm">
          <div class="card-header">
            <h4 class="my-0 font-weight-normal">Milof X 2</h4>
          </div>
          <div class="card-body">
            <h1 class="card-title pricing-card-title">$31 000 <small class="text-muted"><br> x 2 meses</small></h1>
            <ul class="list-unstyled mt-3 mb-4">
              <li>Una cuenta asociada por 2 meses</li>
              <li>Dos categorías de oficios</li>
              <li>Calificación base 2 ★ </li>
              <li>1 capacitación online</li>
              <li><h4 class="btn-link btn-primary" data-toggle="modal" data-target="#exampleModal1">Más!</h4></li>
              <!-- Modal -->
              <div class="modal fade" id="exampleModal1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title" id="exampleModalLabel">Milof x 2</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body">
                      <ul class="list-group">
                        <li class="list-group-item">Además de contar con un perfil en línea podrá acceder a servicios complementarios por DOS meses.</li>
                        <li class="list-group-item">Cree su perfil laboral y empiece a hacer dinero en su tiempo libre</li>
                        <li class="list-group-item">Amplíe su red de contactos laborales</li>
                        <li class="list-group-item">Proporcione recomendaciones calificando a los demás, según el servicio recibido</li>
                        <li class="list-group-item">Busque los perfiles de otros usuarios de MilOficios.</li>
                        <li class="list-group-item">SOPORTE Servicio al cliente.</li>
                        <li class="list-group-item">Su perfil en dos categorías de oficio.</li>
                        <li class="list-group-item">Visualización de datos de perfil de otros usuarios para contactar.</li>
                        <li class="list-group-item">Calificación base: 2 estrellas.</li>
                        <li class="list-group-item">Derecho a 1 curso gratuito SIN certificación, en áreas prestablecidas por el equipo MILOF.</li>
                      </ul>
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                      <button type="button" class="btn btn-primary">Elegir Plan</button>
                    </div>
                  </div>
                </div>
              </div>
            </ul>
            <button type="button" class="btn btn-lg btn-block btn-primary">Elegir Plan</button>
          </div>
        </div>
        <div class="card mb-4 shadow-sm">
          <div class="card-header">
            <h4 class="my-0 font-weight-normal">Milof X 3</h4>
          </div>
          <div class="card-body">
            <h1 class="card-title pricing-card-title">$41000<small class="text-muted"><br> x 3 meses</small></h1>
            <ul class="list-unstyled mt-3 mb-4">
              <li>Una cuenta asociada por 3 meses</li>
              <li>3 categorías de oficios</li>
              <li>Calificación base 2.5 ★ </li>
              <li>1 capacitación online</li>
              <li><h4 class="btn-link btn-primary" data-toggle="modal" data-target="#exampleModal2">Más!</h4></li>
              <!-- Modal -->
              <div class="modal fade" id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title" id="exampleModalLabel">Milof x 3</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body">
                      <ul class="list-group">
                        <li class="list-group-item">Además de contar con un perfil en línea podrá acceder a servicios complementarios por TRES meses.</li>
                        <li class="list-group-item">Cree su perfil laboral y empiece a hacer dinero en su tiempo libre</li>
                        <li class="list-group-item">Amplíe su red de contactos laborales</li>
                        <li class="list-group-item">Proporcione recomendaciones calificando a los demás, según el servicio recibido</li>
                        <li class="list-group-item">Busque los perfiles de otros usuarios de MilOficios.</li>
                        <li class="list-group-item">SEGUIMIENTO Servicio al cliente.</li>
                        <li class="list-group-item">Su perfil en DOS categorías de oficio.</li>
                        <li class="list-group-item">Visualización de datos de perfil de otros usuarios para contactar.</li>
                        <li class="list-group-item">Calificación base: 2.5 estrellas.</li>
                        <li class="list-group-item">Derecho a 1 curso gratuito CON certificación, en áreas prestablecidas por el equipo MILOF.</li>
                      </ul>
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                      <button type="button" class="btn btn-primary">Elegir Plan</button>
                    </div>
                  </div>
                </div>
              </div>
            </ul>
            <button type="button" class="btn btn-lg btn-block btn-primary">Elegir Plan</button>
          </div>
        </div>
        <div class="card mb-4 shadow-sm">
          <div class="card-header">
            <h4 class="my-0 font-weight-normal">Milof X 6</h4>
          </div>
          <div class="card-body">
            <h1 class="card-title pricing-card-title">$79000 <small class="text-muted"><br>x 6 meses</small></h1>
            <ul class="list-unstyled mt-3 mb-4">
              <li>Una cuenta asociada por 6 meses</li>
              <li>4 categorías de oficios</li>
              <li>Calificación base 3 ★ </li>
              <li>2 capacitaciones online</li>
              <li><h4 class="btn-link btn-primary" data-toggle="modal" data-target="#exampleModal3">Más!</h4></li>

              <!-- Modal -->
              <div class="modal fade" id="exampleModal3" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title" id="exampleModalLabel">Milof x 6</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body">
                      <ul class="list-group">
                        <li class="list-group-item">Además de contar con un perfil en línea podrá acceder a servicios complementarios por SEIS meses.</li>
                        <li class="list-group-item">Cree su perfil laboral y empiece a hacer dinero en su tiempo libre</li>
                        <li class="list-group-item">Amplíe su red de contactos laborales</li>
                        <li class="list-group-item">Proporcione recomendaciones calificando a los demás, según el servicio recibido</li>
                        <li class="list-group-item">Busque los perfiles de otros usuarios de MilOficios.</li>
                        <li class="list-group-item">Servicio al cliente personalizado.</li>
                        <li class="list-group-item">Su perfil en TRES categorías de oficio.</li>
                        <li class="list-group-item">Visualización de datos de perfil de otros usuarios para contactar.</li>
                        <li class="list-group-item">Calificación base: 3.5 estrellas.</li>
                        <li class="list-group-item">Publicación destacada(Posición 1 rotativa entre los 4 oficios).</li>
                        <li class="list-group-item">Derecho a 3 curso gratuito CON certificación, en áreas prestablecidas por el equipo MILOF.</li>
                      </ul>
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                      <button type="button" class="btn btn-primary">Elegir Plan</button>
                    </div>
                  </div>
                </div>
              </div>
            </ul>
            <button type="button" class="btn btn-lg btn-block btn-primary">Elegir plan</button>
          </div>
        </div>
        <div class="card mb-4 shadow-sm">
          <div class="card-header">
            <h4 class="my-0 font-weight-normal">Milof X 12</h4>
          </div>
          <div class="card-body">
            <h1 class="card-title pricing-card-title">$143000 <small class="text-muted"><br> x 12 meses</small></h1>
            <ul class="list-unstyled mt-3 mb-4">
              <li>Una cuenta asociada  y destacada por 12 meses.</li>
              <li>4 categorías de oficios</li>
              <li>Calificación base 3.5 ★ </li>
              <li>3 capacitaciones online</li>
              <li><h4 class="btn-link btn-primary" data-toggle="modal" data-target="#exampleModal">Más!</h4></li>

              <!-- Modal -->
              <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title" id="exampleModalLabel">Milof x 12</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body">
                      <ul class="list-group">
                        <li class="list-group-item">Además de contar con un perfil en línea podrá acceder a servicios complementarios por DOCE meses.</li>
                        <li class="list-group-item">Cree su perfil laboral y empiece a hacer dinero en su tiempo libre</li>
                        <li class="list-group-item">Amplíe su red de contactos laborales</li>
                        <li class="list-group-item">Proporcione recomendaciones calificando a los demás, según el servicio recibido</li>
                        <li class="list-group-item">Busque los perfiles de otros usuarios de MilOficios.</li>
                        <li class="list-group-item">Servicio al cliente PERSONALIZADO.</li>
                        <li class="list-group-item">Su perfil en CUATRO categorías de oficio.</li>
                        <li class="list-group-item">Visualización de datos de perfil de otros usuarios para contactar.</li>
                        <li class="list-group-item">Calificación base: 2.5 estrellas.</li>
                        <li class="list-group-item">Derecho a 1 curso gratuito CON certificación, en áreas prestablecidas por el equipo MILOF.</li>
                      </ul>
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                      <button type="button" class="btn btn-primary">Elegir plan</button>
                    </div>
                  </div>
                </div>
              </div>


            </ul>
            <button type="button" class="btn btn-lg btn-block btn-primary">Elegir plan</button>
          </div>
        </div>
      </div>

    
      <footer class="pt-4 my-md-5 pt-md-5 border-top">
        <div class="row">
          <div class="col-12 col-md">
            <small class="d-block mb-3 text-muted">&copy; 2017-2018</small>
          </div>
          <div class="col-6 col-md">
            <h5>Features</h5>
            <ul class="list-unstyled text-small">
              <li><a class="text-muted" href="#">Cool stuff</a></li>
              <li><a class="text-muted" href="#">Random feature</a></li>
              <li><a class="text-muted" href="#">Team feature</a></li>
              <li><a class="text-muted" href="#">Stuff for developers</a></li>
              <li><a class="text-muted" href="#">Another one</a></li>
              <li><a class="text-muted" href="#">Last time</a></li>
            </ul>
          </div>
          <div class="col-6 col-md">
            <h5>Resources</h5>
            <ul class="list-unstyled text-small">
              <li><a class="text-muted" href="#">Resource</a></li>
              <li><a class="text-muted" href="#">Resource name</a></li>
              <li><a class="text-muted" href="#">Another resource</a></li>
              <li><a class="text-muted" href="#">Final resource</a></li>
            </ul>
          </div>
          <div class="col-6 col-md">
            <h5>About</h5>
            <ul class="list-unstyled text-small">
              <li><a class="text-muted" href="#">Team</a></li>
              <li><a class="text-muted" href="#">Locations</a></li>
              <li><a class="text-muted" href="#">Privacy</a></li>
              <li><a class="text-muted" href="#">Terms</a></li>
            </ul>
          </div>
        </div>
      </footer>
    </div>

@endsection
