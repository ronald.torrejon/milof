@extends('layouts.layout2')
@section('title', 'Entra o Registrate')


@section('styles')
    @include('common.head')
@endsection

@section('content')

    @include('login.loginbar')

    <div class='container'>

		<h3><div class="text-center">Términos y condiciones – Miloficios</div></h3><br>
		 
		<p align=justify>Esta página establece los ‘Términos y Condiciones’ bajo los cuales usted puede usar miloficios.com Por favor lea esta página cuidadosamente. Si usted no acepta los términos y condiciones establecidos aquí, no use miloficios.com (el Web Site, la App) ni sus servicios. Mediante el uso de este Web Site usted está indicando la aceptación a estos ‘Términos y Condiciones’. El grupo de empresas (La Compañía)	 que conforman a este portal puede modificar estos 'Términos y Condiciones' en cualquier momento. Usted debe visitar esta página periódicamente para revisar los ‘Términos y Condiciones’ debido a que los mismos son obligatorios para usted. Los términos 'usted' o 'usuario', tal como se usan aquí, se refieren a todas las personas naturales o jurídicas o entidades de cualquier naturaleza que accedan a este Web Site por cualquier razón.</p>


		<br>
		<h3>USO DEL MATERIAL</h3>

		<p align=justify>La Compañía lo autoriza a usted a consultar, revisar y usar el material que se encuentra en el Web Site, únicamente para su uso personal y no comercial y según lo establecido en estos Términos y Condiciones. El contenido de este Web Site, incluyendo pero sin limitarse a los textos, gráficas, imágenes, logotipos, iconos, software y cualquier otro material (el 'Material) están protegidos bajo las leyes colombianas de derechos de autor, leyes de propiedad industrial y otras leyes aplicables. Todo el Material es de propiedad de la Compañía o de sus proveedores o clientes. El uso no autorizado del material puede constituir una violación de las leyes colombianas o extranjeras sobre derechos de autor, leyes de propiedad industrial u otras leyes. Usted no podrá vender o modificar el Material en manera alguna ni ejecutar o anunciar públicamente el Material ni distribuirlo para propósitos comerciales. Usted no podrá copiar o adaptar el código HTML que la Compañía crea para generar sus páginas ni aplicaciones, ya que el mismo está protegido por los derechos de autor de la Compañía.</p>

		<br>
		<h3>USO AUTORIZADO DEL WEB SITE</h3>
		<p align=justify>Reglas Generales: los usuarios no pueden usar el Web Site con el fin de transmitir, distribuir, almacenar o destruir material (i) en violación de cualquier ley aplicable o regulación, (ii) de manera que se infrinjan las leyes sobre derechos de autor, propiedad industrial, secretos comerciales o cualquier otro derecho de propiedad intelectual de terceros o de manera que viole la privacidad, publicidad u otros derechos personales de terceros, o (iii) en forma que sea difamatoria, obscena, amenazante o abusiva.</p>

		<p align=justify>Reglas de Seguridad del Web Site: A los usuarios les está prohibido violar o intentar violar la seguridad del Web Site. Específicamente los usuarios no podrán (i) acceder a información que no esté dirigida o autorizada a dicho usuario o acceder a servidores o cuentas a los cuales el usuario no está autorizado a acceder, (ii) intentar probar la vulnerabilidad de un sistema o red sin la debida autorización o violar las medidas de seguridad o autenticación, (iii) intentar interferir con los servicios prestados a un usuario, servidor o red, incluyendo pero sin limitarse al envió de virus a través del Web Site, (iv) enviar correo electrónico no solicitado, incluyendo promociones y/o publicidad de productos o servicios. La violación de cualquier sistema o red de seguridad puede resultar en responsabilidades civiles y penales. La Compañía investigará la ocurrencia de hechos que puedan constituir violaciones a lo anterior y cooperará con cualquier autoridad competente en la persecución de los usuarios que estén envueltos en tales violaciones.</p>

		<br>
		<h3>USOS PROHIBIDOS DEL WEB SITE</h3>
		<p align=justify>El Web Site puede ser usado únicamente para propósitos legales. La Compañía prohíbe el uso del Web Site en cualquiera de las siguientes formas:</p>

		<p align=justify>-	Incluir en el Web Site cualquier información sobre el producto o servicio a vender u ofrecer, falsa o inexacta o información que no corresponda a la realidad.</p>
		<p align=justify>-	Incluir en el Web Site cualquier derecho de franquicia, esquema de pirámide, membresía a un club o grupo, representación de ventas, agencia comercial o cualquier oportunidad de negocios que requiera un pago anticipado o pagos periódicos, solicitando el reclutamiento de otros miembros, sub-distribuidores o sub-agentes.</p>
		<p align=justify>-	Borrar o revisar cualquier material incluido en el Web Site por cualquiera otra persona o entidad, sin la debida autorización.</p>
		<p align=justify>-	Usar cualquier elemento, diseño, software o rutina para interferir o intentar interferir con el funcionamiento adecuado de este Web Site o cualquier actividad que sea llevada a cabo en el Web Site.</p>
		<p align=justify>-	Intentar descifrar, compilar o desensamblar cualquier software comprendido en el Web Site o que de cualquier manera haga parte del Web Site.</p>
		<p align=justify>-	En general, incluir o colocar en Web Site información falsa, inexacta, incompleta o engañosa.</p>
		<p align=justify>-	Si usted tiene un password o contraseña que le permita el acceso a un área no pública de este Web Site, no podrá revelar o compartir ese password o contraseña con terceras personas o usar el password o contraseña para propósitos no autorizados.</p>

		<br>
		<h3>INFORMACIÓN DE LOS USUARIOS</h3>
		<p align=justify>1.	Usuarios con perfil registrado</p>
		Cuando usted se registra en este Web Site, se le solicitará que suministre a la Compañía cierta información, incluyendo pero sin limitarse a, una dirección válida de correo electrónico (su 'Información'). En adición a los términos y condiciones que puedan ser previstos en otras políticas de privacidad en este Web Site, usted entiende y acuerda que la compañía puede revelar a terceras personas, sobre bases anónimas, cierta información contenida en su solicitud de registro. La Compañía no revelará a terceras personas su nombre, dirección, dirección de correo electrónico o número telefónico sin su consentimiento expresado a través de las diferentes herramientas o sistemas previstos en el Web Site, salvo en la medida en que sea necesario o apropiado para cumplir con las leyes aplicables o con procesos o procedimientos legales en los que tal información sea pertinente. La Compañía se reserva el derecho de ofrecer a usted servicios o productos de la Compañía o de terceras personas, basados en las preferencias que usted haya identificado en su solicitud de registro o en cualquier momento posterior a la misma. Tales ofertas podrán ser hechas por la compañía o por terceras personas.

		<br>
		<p align=justify>2.	Usuario sin registro de perfil, solo suscrito a correos</p>
		<p>Los usuarios que deciden no registrarse en el Web Site pero sí aceptan recibir información del contenido del portal como ofertas de empleo o noticias de interés, entre otros, deberán suministrar un correo electrónico válido para que les llegue esa información, incluyendo las alertas de empleo que se hagan a través del Web Site. Al momento de entregar el correo electrónico se le solicitará la aceptación de estos Términos y Condiciones y del Aviso de Privacidad.</p>

		<p align=justify>El usuario registrado solo a correos recibirá periódicamente un correo con ofertas de empleo o noticias de su interés. Sin embargo, para recibir notificaciones adicionales o modificar la existente deberá registrarse en el Web Site y adquirir un plan. Tampoco podrá aplicar a las ofertas de oficios ni ser visualizado en las búsquedas por las empresas vinculadas al Web Site, hasta que registre su perfil y adquiera un plan. El usuario podrá, en cualquier momento, cancelar su suscripción a través del mismo correo que le llega.</p>

		<br>
		<h3>POLÍTICA Y AVISO DE PRIVACIDAD</h3>
		<p align=justify>En este enlace se encuentra la política de privacidad del Web Site contentiva del aviso de privacidad y del manual interno de tratamiento de datos personales de La Compañía, que se acepta previa y expresamente por usted para los fines que allí se informan de manera previa, expresa e informada, al momento del registro o cuando entrega su correo electrónico como usuario no registrado.</p>

		<p align=justify>En adición a la Política de Privacidad, usted entiende y acuerda que La Compañía puede revelar a terceras personas, sobre bases anónimas, cierta información contenida en su solicitud de registro. La Compañía no revelará a terceras personas su nombre, dirección, correo electrónico, número telefónico u otros datos personales sin su consentimiento expresado a través de las diferentes herramientas o sistemas previstos en el Web Site, salvo en la medida en que sea necesario o apropiado para cumplir con las leyes aplicables o con procesos o procedimientos legales en los que tal información sea pertinente. La Compañía se reserva el derecho de ofrecer a usted servicios o productos de la Compañía o de terceras personas, basados en las preferencias que usted haya identificado en su solicitud de registro o en cualquier momento posterior a la misma. Tales ofertas podrán ser hechas por la compañía o por terceras personas.</p>

		<br>
		<h3>INFORMACIÓN INCLUIDA EN EL WEB SITE POR LOS USUARIOS</h3>
		<p align=justify>Como usuario usted es responsable por sus propias comunicaciones e información y por las consecuencias de incluir o colocar dicha información o comunicaciones en el Web Site. Usted no podrá: (i) incluir o colocar en Web Site material que esté protegido por las leyes sobre derechos de autor, a menos que usted sea el propietario de tales derechos o haya obtenido permiso del propietario de tales derechos para incluir tal material en Web Site, (ii) incluir o colocar en el Web Site material que revele secretos industriales o comerciales, a menos que usted sea el propietario de los mismos o haya obtenido autorización del propietario, (iii) incluir en el Web Site material que de cualquier forma pueda implicar una violación de derechos de propiedad intelectual o industrial o cualquier otro derecho, (iv) incluir material que sea obsceno, difamatorio, abusivo, amenazante u ofensivo para cualquier otro usuario o cualquier otra persona o entidad, (v) incluir en el Web Site imágenes o declaraciones pornográficas o que incluyan sexo explícito o que sea considerada pornografía infantil en los términos de la normativa colombiana o internacional (vi) incluir o colocar en el Web Site publicidad o anuncios publicitarios sin la debida autorización de la Compañía, cadenas de cartas, virus, caballos de troya, bombas de tiempo o cualquier programa de computador o herramienta con la intención de dañar, interferir, interceptar o apropiarse de cualquier sistema, datos o información.</p>

		<p align=justify>La Compañía no otorga garantía alguna, expresa o implícita, acerca de la veracidad, exactitud o confiabilidad de la información incluida en el Web Site por los usuarios ni apoya o respalda las opiniones expresadas por los usuarios. Usted reconoce y declara que la confianza por usted depositada en cualquier material incluido en el Web Site por los usuarios se hará bajo su propio riesgo.</p>

		<p align=justify>La Compañía actúa como un medio pasivo para la distribución y publicación en Internet de información presentada por los usuarios y no tiene obligación de revisar anticipadamente tal información ni es responsable de revisar o monitorear la información incluida en Web Site por los usuarios. Si la Compañía es notificada por un usuario acerca de la existencia de información que no cumpla con estos Términos y Condiciones, la Compañía puede investigar tal información y determinar de buena fe y a su exclusiva discreción si remueve o elimina tal información o solicita que sea removida o eliminada del Web Site. La Compañía se reserva el derecho de expulsar usuarios o de prohibir su acceso futuro al Web Site por violación de estos Términos y Condiciones o de la ley aplicable. Igualmente, la Compañía se reserva el derecho de eliminar del Web Site información presentada o incluida por un usuario, cuando lo considere apropiado o necesario a su exclusiva discreción, si estima o cree que tal información puede generar responsabilidad para la Compañía o puede causar la pérdida de los servicios de sus proveedores de internet (ISPs) o de otros proveedores.</p>

		<br>
		<h3>REGISTRO Y CONTRASEÑA (PASSWORD)</h3>
		<p align=justify>Usted es responsable por mantener la confidencialidad de su password o contraseña. Usted será responsable por todos los usos de su registro en Web Site, sean o no autorizados por usted. Usted acuerda notificar inmediatamente a la Compañía cualquier uso no autorizado de su registro y password o contraseña.</p>

		<p align=justify>El usuario podrá reestablecer la contraseña asociada con su cuenta de miloficios.com mediante: (i) el envío de mensajes de texto, desde la plataforma al móvil registrado en la cuenta, o (ii) a través de la recuperl reconocimiento óptico de caracteres del documento de identidad del usuario. Para el proceso de reconocimiento óptico de caracteres, el usuario deberá adjuntar su documento de identidad cuando el sistema le indique, a fin de validar la información contenida en el mismo y permitir el cambio de correo electrónico. En ningún caso el documento de identidad ingresado en la plataforma de miloficios.com, será almacenado o usado para otros fines distintos a los indicados en el presente título, por lo que se procederá a su eliminación inmediata una vez finalizado el procedimiento.”</p>

		<br>
		<h3>PROHIBICIÓN DE REVENTA, CESIÓN O USO COMERCIAL NO AUTORIZADO</h3>
		<p align=justify>Usted acuerda no revender o ceder sus derechos u obligaciones bajo estos Términos y Condiciones. Usted acuerda igualmente no hacer un uso comercial no autorizado de este Web Site.</p>

		<br>
		<h3>TERMINACIÓN</h3>
		<p align=justify>La Compañía se reserva el derecho, a su exclusiva discreción, de borrar toda la información que usted haya incluido en el Web Site y de terminar inmediatamente su registro, acceso al Web Site o a determinados servicios proveídos por la Compañía, ante el incumplimiento por su parte de estos Términos y Condiciones o ante la imposibilidad de verificar o autenticar cualquier información que usted haya presentado en su forma de registro para acceder al Web Site.</p>

		<br>
		<h3>DISPOSICIONES GENERALES</h3>
		<p align=justify>La Compañía no asegura que el material pueda ser legalmente accesado o visto fuera del territorio de la República de Colombia. El acceso al Material puede no ser legal por ciertas personas o en ciertos países. Si usted tiene acceso a este Web Site desde un lugar ubicado fuera del territorio de la República de Colombia, lo hace bajo su propio riesgo y es responsable del cumplimiento de las leyes aplicables en su jurisdicción. Estos T érminos y Condiciones están regidos por las leyes de la República de Colombia, sin dar aplicación a las normas o principios sobre conflicto de leyes. La jurisdicción para cualquier reclamación que surja de estos Términos y Condiciones será exclusivamente la de los tribunales y jueces de la República de Colombia. Si alguna previsión de estos términos y condiciones es declarada nula, inválida o ineficaz, ello no afectará la validez de las restantes previsiones de estos Términos y Condiciones.</p>



		<br>
		<h3>RESPONSABILIDAD DE LA COMPAÑÍA</h3>
		<p align=justify>La compañía actúa solamente como un lugar o escenario para que la oferta y demanda de los servicios de empleo y relacionados, incluyan o publiquen sus propuestas y servicios. La Compañía no revisa ni censura las ofertas, bienes o servicios publicados. La Compañía no está involucrada ni se involucra en las transacciones o tratos entre la oferta y demanda y/o los usuarios de este sitio web. La selección de uno y otro servicio y/o producto es responsabilidad exclusiva de los usuarios. La compañía no avala, certifica, garantiza ni recomienda los productos y servicios ofrecidos a través del portal. La Compañía no tiene ni ejerce control alguno sobre la calidad, idoneidad, seguridad o legalidad de los servicios y/o productos ofrecidos o publicados en su portal. La Compañía tampoco tiene ni ejerce control alguno sobre la veracidad o exactitud de la información publicada por los oferta y la demanda del servicio, sobre los productos y/o servicios ofrecidos.</p>

		<p align=justify>Adicionalmente por favor tenga en cuenta que existen riesgos, que incluyen pero no se limitan al daño o lesiones físicas, al tratar con extraños, menores de edad o personas que actúan bajo falsas pretensiones. Usted asume todos los riesgos asociados con tratar con otros usuarios con los cuales usted entre en contacto a través del portal. El cumplimiento de los términos y condiciones de los contratos o acuerdos que usted llegue a celebrar con otros usuarios del web site, así como el cumplimiento de las leyes aplicables a tales contratos o acuerdos, es responsabilidad exclusiva de los usuarios y no de la Compañía. Debido a que la autenticidad de los usuarios de internet es difícil, la Compañía no puede confirmar y no confirma que cada usuario es quien dice ser. Debido a que la Compañía no se involucra en las relaciones o tratos entre sus usuarios ni controla el comportamiento de los usuarios o participantes en el portal, en el evento en que usted tenga una disputa con uno o más usuarios del web site, usted libera a la Compañía (y a sus empleados y agentes) de cualquier reclamación, demanda o daño de cualquier naturaleza, que surja de o de cualquier otra forma se relacione con dicha disputa.</p>

		<p align=justify>La Compañía no controla la información suministrada por otros usuarios y que pueda estar disponible a través del Web Site. Por su propia naturaleza la información proveniente de terceras personas puede ser ofensiva, dañina, falsa o inexacta y en algunos casos puede ser titulada o rotulada de manera errónea o decepcionante. La Compañía espera que usted emplee la debida precaución y sentido común cuando use este Web Site. El Material puede contener inexactitudes o errores tipográficos. La Compañía no otorga garantía alguna, expresa o implícita, acerca de la precisión, exactitud, confiabilidad u oportunidad del Web Site o del Material. Nada de lo incluido en Web Site por la Compañía o por los usuarios constituye recomendación, asesoría o consejo suministrado por la Compañía. El uso del Web Site y del material, al igual que las decisiones que usted adopte con base en este Web Site y el Material, se hacen bajo su propio y exclusivo riesgo. La Compañía recomienda que todas las decisiones que usted pretenda adoptar con base en el Material y cualquier otra información incluida en el Web Site sean consultadas con sus propios asesores y consultores. La Compañía no será responsable por cualquier decisión de compra o negocio que usted tomo con base en uso de este WebSite.</p>

		<p align=justify>La compañía no garantiza que el web site opere libre errores o que el web site y su servidor se encuentre libre de virus de computadores u otros mecanismos dañinos. Si el uso del web site o del material resulta en la necesidad de prestar servicio de reparación o mantenimiento a sus equipos o información o de reemplazar sus equipos o información, la compañía no es responsable por los costos que ello implique.</p>

		<p align=justify>El web site y el material se ponen a disposición en el estado en que se encuentren. La compañía no otorga garantía alguna sobre la exactitud, confiabilidad u oportunidad del material, los servicios, los textos, el software, las gráficas y los links o vínculos.</p>

		<p align=justify>En ningún caso la compañía, sus proveedores o cualquier persona mencionada en el web site será responsable por daños de cualquier naturaleza, resultantes del uso o la imposibilidad de usar el web site o el material.</p>

		<p align=justify>LINKS A OTROS WEB SITES. El Web Site contiene links o vínculos a web sites de terceras personas. Estos links o vínculos se suministran para su conveniencia únicamente y la Compañía no respalda, recomienda o asume responsabilidad alguna sobre el contenido de los web sites de terceras personas. Si usted decide acceder a través de los links o vínculos a los web sites de terceras personas, lo hace bajo su propio riesgo y responsabilidad.</p>

		<br>
		<h4>PROMOCIONES, CONCURSOS Y EVENTOS</h4>
		<p align=justify>Las promociones, concursos, sorteos y eventos que se implementen en el Portal estarán sujetas a las reglas y condiciones que en cada oportunidad se establezca por parte de La Compañía, siendo necesario como requisito mínimo para acceder a tales oportunidades o beneficios comerciales, que el Usuario se encuentre debidamente registrado como usuario del Web Site. La Compañía no se responsabiliza por cualquier tipo de daño -incluyendo moral, físico, material, ni de cualquier otra índole- que pudiera invocarse como relacionado con la recepción por parte del usuario registrado de cualquier tipo de obsequios y/o regalos remitidos por La compañía y/o cualquier aliado de este. Así mismo, La Compañía no será responsable por las consecuencias que pudieren causar el ingreso al Web Site y/o la presencia en cualquier evento y/o reunión organizada por éste. El Usuario reconoce que La Compañía no asume responsabilidad alguna que corresponda a un anunciante y/o el proveedor de los servicios que se ofrezcan en el Web Site, siendo entendido que La Compañía no se responsabiliza por la calidad ni la entrega de los productos o prestación de servicios que se publican en este sitio. Por tal motivo no será responsable por cualquier problema, queja o reclamo de los usuarios por cuestiones atinentes a dichos productos y/o servicios.</p>

		<p align=justify>Cada promoción, concurso o evento que se promueva o realice a través del Web Site, estará sujeto a las reglas de Privacidad que para el mismo se indiquen, por lo que la participación en los mismos deberá atenerse a lo que en cada caso se señale, lo cual será complementario a las políticas de privacidad señaladas anteriormente, siempre y que no sea excluyente.</p>

		<br>
		<h3>BASES DE DATOS E INFORMACIÓN</h3>
		<p align=justify>Quien diligencia el formulario de registro autoriza de modo expreso a La compañía  (G4 Consultoría, comunicaciones y eventos SAS e Ingeniería Verde & Energías Renovables) y sus filiales o subsidiarias para recolectar, procesar y comercializar los datos contenidos en el mismo. Con base en lo anterior, La compañía, sus filiales o subsidiarias podrán reproducir, publicar, traducir, adaptar, extraer o compendiar los datos o información suministrada. Del mismo modo le confiere la facultad para disponer de ellos a título oneroso o gratuito bajo las condiciones lícitas que su libre criterio dicte. A su vez, quien diligencia el formulario de registro declara que conoce y acepta que los datos contenidos en el mismo pueden ser utilizados para impulsar, dirigir, ejecutar y de manera general, llevar a cabo campañas promocionales o concursos de carácter comercial o publicitario, de La compañía y sus filiales o subsidiarias o de otras personas o sociedades con quien esta contrate tales actividades, mediante el envío de Email, Mensaje de texto (SMS y/o MMS) o a través de cualquier medio análogo y/o digital de comunicación.</p>

		<p align=justify>Al usuario que adquiera el servicio de Planes Milof y/o cualquier otro a través de medios no tradicionales, le asiste el derecho de retracto, el cual podrá ejercer dentro de los cinco (5) días hábiles siguientes a la fecha del pago, salvo que antes de ejercer este derecho haya comenzado a usar el servicio de conformidad con lo establecido en el artículo 47 de la Ley 1480 de 2011. Para el servicio de Planes MILOF serán aplicables estos Términos y Condiciones en lo que no contradigan los específicos para ese servicio.</p>

		<br>
		<h3>Términos y Condiciones – Planes MILOF</h3>
		<p align=justify>Acerca de Planes MILOF:</p>
		<p align=justify>Los Planes MILOF son una herramienta que le ayudarán a aumentar la visibilidad del perfil, así como el poder adquirir mayores categorías que complementarán, así como la elección de cursos que contribuyan al mejoramiento del perfil.</p>

		<p align=justify>-	Prioridad en búsquedas</p>
		<p align=justify>Si cumple con el perfil requerido, su hoja de vida aparecerá en un lugar preferencial en las búsquedas de candidatos que se realicen durante el tiempo que haya adquirido el Plan MILOF, en conjunto, se convertirá en el mejor aliado para encontrar un empleo.</p>
		<p align=justify>* La prioridad en la búsqueda solo comenzará cuando haya adquirido uno de los Planes MILOF.</p>

		<p align=justify>-	Más Categorías</p>
		<p align=justify>Con este servicio, de acuerdo al Plan MILOF adquirido, usted podrá disfrutar de más opciones para buscar un empleo ocasional, durante el tiempo adquirido.</p>
		<p align=justify>-	Cursos Online</p>
		<p align=justify>De acuerdo al Plan MILOF adquirido, podrá llevar un curso en línea, ofrecido por un experto de la temática por única vez y certificado por las empresas que conforman a La Compañía.</p>
		<p align=justify>Al usuario que adquiera el servicio de los Planes MILOF le asiste el derecho de retracto, el cual podrá ejercer dentro de los cinco (5) días hábiles siguientes a la fecha del pago, salvo que antes de ejercer este derecho haya comenzado a usar el servicio de conformidad con lo establecido en el artículo 47 de la Ley 1480 de 2011. </p>


		<br>
		<h4>I.	¿Cómo funciona?</h4>
		<p align=justify>Compre alguno de los Planes MILOF a través de los medios habilitados en nuestra página web. Cuando La Compañía haya recibido su pago, se habilitará el plan adquirido con todos los servicios adicionales.</p>

		<br>
		<h3>II.	CONDICIONES Y RESTRICCIONES</h3>
		<p align=justify">Los servicios de Prioridad en búsquedas, Más Categorías y Cursos Online estarán incluidos según el Plan que haya adquirido el usuario. Dichos servicios estarán disponibles únicamente para personas cuyo perfil esté registrado en miloficios.com y que compren algunos de los Planes MILOF a través de los medios de pago definidos a partir del 2 de enero de 2019.</p>
		<p>La compra de estos servicios no pueden ser cedidos, revendidos, o transferidos a cualquier título en favor de terceros.</p>
		<p>Los servicios adicionales de los Planes MILOF no garantizan que la persona consiga un empleo / o un mejor empleo / o que lo llamen a acordar servicios. Solo mejora la información del perfil y amplía el rango de oficios registrados en miloficios.com
		Si bien al inicio de su suscripción existirán varios contactos entre miloficios y el cliente para programar el uso de los servicios adicionales, en especial el del curso online, en caso que este no lo requiera en ese momento, entiende que podrá hacerlo dentro del año siguiente a la activación de la membresía, enviando una comunicación por los canales previstos por miloficios para solicitar este servicio.</p>

	</div>
@stop

@section('scripts')

@stop