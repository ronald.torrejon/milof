<div class="container" >
    <form class="form-group" method="POST" action="/users" enctype="multipart/form-data">
      @csrf
       <div class="row">
          <div class="col-md-5 order-md-2 mb-1">
              <h4 class="d-flex justify-content-between align-items-center mb-3">
                <span class="text-muted">Registrate Aquí</span>
                <span class="badge badge-secondary badge-pill">YO SOY MILOF</span>
              </h4>
              <img src={{ asset('milof.jpg') }} class="img-fluid" alt="Responsive image">
          </div>
            
          <div class="col-md-7 order-md-1"> 
                
               <br>
                <div class="card">
                  <div class="card-header" id="headingOne">
                    <h5 class="mb-0">
                  
                        <a class="">
                        Registrar Oficio
                        </a>
                  
                    </h5>
                  </div>
                  <div class="">
                      <div class="card card-body">

                        <!-- Carga archivo(Fila 7): Foto -->
                        
                        
                          <label for="validationDefault01">Oficio</label>
                          <input type="text" name="job" class="form-control" id="validationDefault01" placeholder="Ejemplo: Carpintero"  >
                          <BR> 
                          <!-- Inicio  Contenedor 6(Fila 6): Presentación -->
                          
                               
                          <label for="validationDefault01">Presentación o Perfil(Máximo 200 caracteres)</label>
                          <textarea rows="5" name="resume" class="form-control" cols="80" wrap="soft" maxlength="250"></textarea>
                          <BR>      
                          
                          <!--  Fin  Contenedor 6(Fila 6): Presentación -->
                          <p><label for="">Foto</label></p>  
                          <input type="file"  name= "photo" >

                      </div>
                  </div>
                </div>
                <div class="form-group">
                      <div class="form-check">
                        <input class="form-check-input" type="checkbox" value="" id="invalidCheck2" required>
                        <label class="form-check-label" for="invalidCheck2">
                         Acepto términos y condiciones
                        </label>
                      </div>
                </div>
                <!-- Fin  Contenedor 8(Fila 9): Check de términios y condiciones -->
            
                <!-- boton de registro -->
                <button class="btn btn-primary" type="submit">Registrarse</button>
          </div>
      </div>
      
    </form>
  </div>
  <footer class="my-5 pt-5 text-muted text-center text-small">
        <p class="mb-1">&copy; 2017-2018 Company Name</p>
        <ul class="list-inline">
          <li class="list-inline-item"><a href="#">Privacy</a></li>
          <li class="list-inline-item"><a href="#">Terms</a></li>
          <li class="list-inline-item"><a href="#">Support</a></li>
        </ul>
    </footer>
  