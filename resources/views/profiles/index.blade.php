@extends('layouts.layout2')
@section('title', 'Lista')

@section('styles')
      @include('common.head')
@stop
@section('content')

  @include('login.loginbar')
  @include('profiles.search')
  
  <div class="container">

  {!!$profiles->render(); !!}
     <div class="row">
        @foreach($profiles as $profile)
         
            <div class="col-sm">
              <div class="card text-center" style="width: 18rem; margin-top:50px;">
                    <div class="card-body">
                      <h5 class="card-title">{{$profile->getPosterUsername()}}</h5>
                      @if($profile->photo)

                        <img style="height: 150px; width: 150px " src="{{$profile->photo}}" class="card-img-top rounded-circle">
                      @endif
                      <br>
                      <h5><label for="validationDefault01">{{$profile->qualification}}</label></h5>
                      <div id="rate2"></div> 
                      
                      <h5><label for="validationDefault01">{{$profile->getPosterJobname()}}</label></h5>
                      
                      <div class="col-md-12 mb-1">
                            
                            {{$profile->resume}}
                      </div>
                      
                      <a href="/profiles/{{$profile->slug}}" class="btn btn-primary">Ver más...</a>
                    </div>
              </div>
            </div>
            
        @endforeach
    </div>
   {!!$profiles->render(); !!} 
  </div>

  
@endsection
