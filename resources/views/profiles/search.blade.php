<nav class="navbar navbar-expand-lg navbar-light "  style="background-color: #E5E8E8;">

              <div class="container">
              <select class="custom-select" id="inputGroupSelect01">
                <option selected>Oficio...</option>
                @foreach($jobsall as $variable)
                  
                  <div class="col-md-12 mb-1">
                            
                        <option> {{$variable->name}}</option>
                  </div>
                  
                @endforeach
              </select>
            </div>

              <div class="col-md-0">

                  <input class="form-control" type="text" id="minimo" name="minimo" readonly ></label>
              </div>
              <div class="col-md-2" >
                   <label for="amount">Rango de edad:</label>
                   <div id="slider-range"></div>
              </div>
              <div class="col-md-0 ">
                  <input class="form-control" type="text" id="maximo"  name="maximo"readonly >
              </div>
              <div class="container">
                <div class="form-check form-check-inline">
                  <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio1" value="option1">
                  <label class="form-check-label" for="inlineRadio1">Masculino</label>
                </div>
                <div class="form-check form-check-inline">
                  <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio2" value="option2">
                  <label class="form-check-label" for="inlineRadio2">Femenino</label>
                </div>
                <div class="form-check form-check-inline">
                  <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio3" value="option3">
                  <label class="form-check-label" for="inlineRadio3">Otro </label>
                </div>
              </div>
              

              <select class="form-control mr-sm-3"  id="radio">
                    <option selected>Radio de busqueda</option>
                    <option value="1">1 Km</option>
                    <option value="2">2 Km</option>
                    <option value="3">4 Km</option>
                    <option value="4">10 Km</option>
                    <option value="5">Ciudad</option>
                    <option value="6">Departamento</option>
                    <option value="7">País</option>
              </select>
              <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Buscar</button>     
</nav>