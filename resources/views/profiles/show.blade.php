<html>
  <head>
      <title>1000ficios - Seleccionado</title>

      @include('common.head')

  </head>
  <body>

    
    @include('login.loginbar')
    @include('profiles.search')
    <div class="text-center">

      <h5><label for="validationDefault01">{{$profile->getPosterJobname()}}</label></h5>
      @if($profile->photo)

         <img style="height: 150px; width: 150px " src="{{$profile->photo}}" class="card-img-top rounded-circle">
      @endif

      
      <h5 class="card-title">{{$profile->getPosterUsername()}}</h5>
      <div class="row justify-content-md-center" id="rate2"></div>
      <br>
      <br>
    </div>
    <div class='container'>
        <div class="col-md-6 mb-1">
                <label class="h6" for="validationDefault01">Nombre:  </label>
                {{$profile->getPosterUsername()}}  {{$profile->getPosterUsersurname()}}
        </div>
        <div class="col-md-6 mb-1">
                <label class="h6" for="validationDefault01">Documento:  </label>
                {{$profile->getPosterUsertype_doc()}}  N° {{$profile->getPosterUsernum_doc()}}
        </div>  
        <div class="col-md-8 mb-1">
                <label class="h6" for="validationDefault01">Perfil:  </label>
                {{$profile->resume}}
        </div>
        <div class="col-md-4 mb-1">
                <label class="h6" for="validationDefault01">N° Móvil:  </label>
                {{$profile->getPosterUsermobile()}}
        </div>    
        <div class="col-md-4 mb-1">
                <label class="h6" for="validationDefault01">E-mail:  </label>
                {{$profile->getPosterUseremail()}}
        </div>  
        <div class="col-md-4 mb-1">
                <label class="h6" for="validationDefault01">Fecha Nac:  </label>
                {{$profile->getPosterUserbirth()}}
        </div> 
        <div class="col-md-4 mb-1">
                <label class="h6" for="validationDefault01">Edad:  </label>
                {{Carbon::createFromDate(date("Y", strtotime($profile->getPosterUserbirth())),date("m", strtotime($profile->getPosterUserbirth())),date("d", strtotime($profile->getPosterUserbirth())))->age}} años
        </div>

          
            
            <div class="col-md-4 mb-1">    
            <label class="h6" for="validationDefault01">Calificación:  </label>
            {{$profile->qualification}}

            <div id="rate2"></div> 
        </div>
    </div>
        
    @include('common.script1')
    
  </body>
</html>

