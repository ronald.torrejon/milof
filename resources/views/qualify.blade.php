
<html>
<head>
    <title>starRating, star rating jquery plugin</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>
    <script src="https://google-code-prettify.googlecode.com/svn/loader/run_prettify.js"></script>
    <link rel="stylesheet" type="text/css" href="css/star-rating-svg.css">
    <link rel="stylesheet" type="text/css" href="css/demo.css">

</head>
<body>

  <div class="content">
  
 

  <h4>Calificar</h4>

  <!-- example using callback -->
  <span class="my-rating"></span>
  <span class="live-rating"></span>



  <script>
    $(function() {
    
      $(".my-rating").starRating({
        initialRating: 1.5,
        
        disableAfterRate: false,
        starShape: 'rounded',

        onHover: function(currentIndex, currentRating, $el)
        {
          console.log('index: ', currentIndex, 'currentRating: ', currentRating, ' DOM element ', $el);
          $('.live-rating').text(currentIndex);
        },
        onLeave: function(currentIndex, currentRating, $el)
        {
          console.log('index: ', currentIndex, 'currentRating: ', currentRating, ' DOM element ', $el);
          $('.live-rating').text(currentRating);
        }


      });

  });

</script>
<script src="js/jquery.star-rating-svg.js"></script>

</body>
</html>