 <div class="container" >
    <form class="form-group" method="POST" action="/users">
      @csrf
       <div class="row">
          <div class="col-md-5 order-md-2 mb-1">
              <h4 class="d-flex justify-content-between align-items-center mb-3">
                <span class="text-muted">Registrate Aquí</span>
                <span class="badge badge-secondary badge-pill">YO SOY MILOF</span>
              </h4>
              <img src={{ asset('milof.jpg') }} class="img-fluid" alt="Responsive image">
          </div>
            
          <div class="col-md-7 order-md-1"> 
                <!-- Inicio Contenedor 1(Fila 1): Nombres y apellidos -->
                <div class="form-row">
                    <div class="col-md-4 mb-1">
                      <label for="validationDefault01">Nombres</label>
                      <input type="text" name="names" class="form-control" id="validationDefault01" placeholder="Ejemplo: Ronald Paul"  required>
                    </div>
                    <div class="col-md-4 mb-3">
                      <label for="">Apellidos</label>
                      <input type="text" name="surnames"class="form-control" id="validationDefault02" placeholder="Ejemplo: Torrejón Infante"  required>
                    </div>
                    <div class="col-md-4 mb-2">
                      <label for="validationDefault01">E-mail</label>
                        <input type="email" name="email"class="form-control" id="validationDefault05" placeholder="Ejemplo: 3156767142" aria-describedby="inputGroupPrepend2" required>                  
                    </div>      
                </div>
                <!-- Fin  Contenedor 1(Fila 1): Nombres y apellidos -->

                <!-- Inicio  Contenedor 2(Fila 1): Tipo de documento, número de documento, fecha de nacimiento -->
                <div class="form-row">
                            <div class="col-md-4 mb-1">
                              <label for="validationDefault01">Tipo doc</label>
                              <select class="form-control"name="type_doc" id="exampleFormControlSelect1">
                                  <option>CC</option>
                                  <option>CI</option>
                                  <option>DNI</option>
                                  <option>DNIC</option>
                                  <option>CE</option>
                                  <option>PASAPORTE</option>
                                  <option>OTRO</option>
                              </select>
                            </div>

                            <div class="col-md-4 mb-2">
                                <label for="validationDefault02">N° Documento</label>
                                <input type="text" name="num_doc"class="form-control" id="validationDefault04" placeholder="Ejemplo: 518259"  required>
                            </div>
                            <div class="col-md-4 mb-3">
                                <label for="validationDefaultUsername">Fecha Nacimiento</label>
                                <div class="input-group">
                                    <input class="date form-control"  type="text" id="startdate" name="birth" placeholder="Ejemplo:1979-09-21">
                                    
                                </div>
                            </div>
                </div>
                <!-- Fin  Contenedor 2(Fila 1): Tipo de documento, número de documento, fecha de nacimiento -->

                <!-- Inicio  Contenedor 3(Fila 3): Pais, departamento, localidad -->
                <div class="form-row">
                      <div class="col-md-4 mb-3">
                          <label for="validationDefault03">Pais</label>
                          <select class="form-control" name="country" id="exampleFormControlSelect1">
                                  <option>BOLIVIA</option>
                                  <option>CHILE</option>
                                  <option>COLOMBIA</option>
                                  <option>ECUADOR</option>
                                  <option>PERU</option>
                                  <option>OTRO</option>
                          </select>
                      </div>
                      <div class="col-md-3 mb-3">
                          <label for="validationDefault04">Departamento</label>

                          <select class="form-control" name="div_1" id="exampleFormControlSelect1">
                                  <option>AMAZONAS</option>
                                  <option>ANTIOQUIA</option>
                                  <option>ARAUCA</option>
                                  <option>ATLANTICO</option>
                                  <option>BOLIVAR</option>
                                  <option>BOYACA</option>
                                  <option>CALDAS</option>
                                  <option>CAQUETA</option>
                                  <option>CASANARE</option>
                                  <option>CAUCA</option>
                                  <option>CESAR</option>
                                  <option>CHOCO</option>
                                  <option>CORDOVA</option>
                                  <option>CUNDINAMARCA</option>
                                  <option>GUAINIA</option>
                                  <option>GUAVIARE</option>
                                  <option>HUILA</option>
                                  <option>LA GUAJIRA</option>
                                  <option>MAGDALENA</option>
                                  <option>META</option>
                                  <option>NARIÑO</option>
                                  <option>NORTE DE SANTANDER</option>
                                  <option>PUTUMAYO</option>
                                  <option>QUINDIO</option>
                                  <option>RISARALDA</option>
                                  <option>SAN ANDRES Y PROVIDENCIA</option>
                                  <option>SANTANDER</option>
                                  <option>SUCRE</option>
                                  <option>TOLIMA</option>
                                  <option>VALLE DEL CAUCA</option>
                                  <option>VAUPES</option>  
                                  <option>VICHADA</option> 
                          </select>
                      </div>
                        <div class="col-md-5 mb-3">
                          <label for="validationDefault05">Ciudad</label>
                          <input type="text" name="location" class="form-control"  id="validationDefault05" placeholder="Ejemplo: Villavicencio" required>
                        </div>
                </div>
                <!-- Fin  Contenedor 3(Fila 3): Pais, departamento, ciudad -->
            
                <!-- Inicio  Contenedor 4(Fila 4): Dirección, número de celular, sexo -->
                <div class="form-row ">
                      <div class="col-md-5 mb-3">
                        <label for="validationDefault01">Dirección</label>
                        <input type="text" name="address" class="form-control" id="validationDefault01" placeholder="Ejemplo: AV. Grau 1399" required>
                      </div>
                      <div class="col-md-3 mb-3">
                          <label for="validationDefault01">N° Celular</label>
                          <input type="text" name="mobile" class="form-control" id="validationDefault05" placeholder="Ej: 3156767142" aria-describedby="inputGroupPrepend2" required>                  
                      </div>
                      <div class="col-md-4 mb-3">
                          <label for="validationDefault01">Sexo</label>
                          <br>
                          <div class="form-check form-check-inline">
                              <input class="form-check-input" type="radio" name="sex" id="sexo1" value="Hombre" required>
                              <label class="form-check-label" for="sexo1">Hombre</label>
                          </div>
                          <div class="form-check form-check-inline">
                              <input class="form-check-input" type="radio" name="sex" id="sexo2" value="Mujer" required>
                              <label class="form-check-label" for="sexo2">Mujer</label>
                          </div>
                          <div class="form-check form-check-inline">
                              <input class="form-check-input" type="radio" name="sex" id="sexo3" value="Otro" required>
                              <label class="form-check-label" for="sexo3">Otro </label>
                          </div>
                      </div>
                </div>
                <!-- Fin  Contenedor 4(Fila 4): Dirección, número de celular, sexo -->
            
                <!-- Inicio  Contenedor 5(Fila 5): Oficio -->
                <div class="form-row ">
                    <!-- 
                    <div class="col-md-6 mb-3">
                        <label for="validationDefault02"> Mi Oficio </label>
                        <input type="text" name="oficio1"class="form-control" id="validationDefault02" placeholder="Ejemplo: Niñera" required>
                    </div>
                    -->
                    
                </div>


                <div class="form-row ">
                    <div class="col-md-4 mb-3">
                        <label for="validationDefault01">Contraseña</label>
                        <input type="password" name="password"class="form-control" id="validationDefaultUsername" placeholder="Ejemplo: 12345rfdg"  required>
                    </div>
                    <div class="col-md-4 mb-3">
                        <label for="validationDefault02">Confirmar Contraseña</label>
                        <input type="password" name="password2"class="form-control" id="validationDefault02" placeholder="Ejemplo: 12345rfdg"  required>
                    </div>
                    <div class="col-md-4 mb-3">
                        <label for="validationDefault02"> N° Referido </label>
                        <input type="text" name="referred"class="form-control" id="validationDefault02" placeholder="Ejemplo: AA1" required>
                    </div>
                </div>
                <!-- Fin  Contenedor 7(Fila 8): Contraseña -->

            
                <!-- Inicio  Contenedor 8(Fila 9): Check de términios y condiciones -->
                <div class="form-group">
                      <div class="form-check">
                        <input class="form-check-input" type="checkbox" value="" id="invalidCheck2" required>
                        <label class="form-check-label" for="invalidCheck2">
                         <a href="{{ url('/politicas') }}"> Acepto términos y condiciones</a>
                        </label>
                      </div>
                </div>
                <!-- Fin  Contenedor 8(Fila 9): Check de términios y condiciones -->
            
                <!-- boton de registro -->
                <button class="btn btn-primary" type="submit">Registrarse</button>
          </div>
      </div>
      
    </form>
  </div>
  <footer class="my-5 pt-5 text-muted text-center text-small">
        <p class="mb-1">&copy; 2017-2018 Company Name</p>
        <ul class="list-inline">
          <li class="list-inline-item"><a href="#">Privacy</a></li>
          <li class="list-inline-item"><a href="#">Terms</a></li>
          <li class="list-inline-item"><a href="#">Support</a></li>
        </ul>
    </footer>
    
    <script type="text/javascript">  
            $('#startdate').datepicker({ 
                autoclose: true,   
                format: 'yyyy-mm-dd'  
             });
             $('#enddate').datepicker({ 
                autoclose: true,   
                format: 'yyyy-mm-dd'
             }); 
    </script>