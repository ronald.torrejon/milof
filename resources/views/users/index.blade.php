@extends('layouts.layout2')
@section('title', 'Lista')

@section('styles')
  @include('common.head')
@endsection


@section('content')
  
 @include('login.loginbar')
 @include('profiles.search')
  <div class="container">
     <div class="row">
        @foreach($users as $user)
         
            <div class="col-sm">
              <div class="card text-center" style="width: 18rem; margin-top:50px;">
                    <div class="card-body">
                      <h5 class="card-title">{{$user->names}}</h5>
                      <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                      <a href="/users/{{$user->slug}}" class="btn btn-primary">Ver más...</a>
                    </div>
              </div>
            </div>
         
        @endforeach
      </div>
  </div>

  {!!$users->render(); !!}
@endsection
