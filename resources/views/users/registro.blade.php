@extends('layouts.layout2')
@section('title', 'Registro')
@section('styles')
  @include('common.head')
@endsection

@section('content')
  @include('login.loginbar')
  @include('users.form')
@endsection
