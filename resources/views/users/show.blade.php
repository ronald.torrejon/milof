@extends('layouts.layout2')
@section('title', 'Asociado')
@section('styles')
  @include('common.head')
@endsection
@section('content')
  <header>
                <div class="navbar navbar-dark bg-info shadow-sm">
                    <div class="col-md-4 ">
                        <a href="#" class="navbar-brand d-flex align-items-center">
                            <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="mr-2"><path d="M23 19a2 2 0 0 1-2 2H3a2 2 0 0 1-2-2V8a2 2 0 0 1 2-2h4l2-3h6l2 3h4a2 2 0 0 1 2 2z"></path><circle cx="12" cy="13" r="4"></circle></svg>
                            <strong>1000ficios</strong>
                        </a>
                       

                        
                    </div>
                     <form class="col-sm-4 offset-md-2">
                          <div class="row">
                            <div class="col">
                              <input type="text" class="form-control" placeholder="E-mail">
                            </div>
                            <div class="col">
                              <input type="text" class="form-control" placeholder="Contraseña">
                            </div>
                          </div>
                        </form>

                    <div class="col-sm-1">
                            <a class="btn btn-outline-dark" href="#">Entrar</a>
                    </div>
                </div>
  </header>

  @include('profiles.search')
  <div class="text-center">

    <h5 class="card-title">{{$user->names}}</h5>
 </div>
    <p class="card-text">Ejemplo text to build on the card title and make up the bulk of the card's content.</p>
    <div class="col-md-4 mb-1">
          <label for="validationDefault01">Nombres:  </label>
          {{$user->names}}
    </div>
    <div class="col-md-4 mb-1">
          <label for="validationDefault01">Apellidos:  </label>
          {{$user->surnames}}
    </div>
    <div class="col-md-4 mb-1">
          <label for="validationDefault01">E-mail:  </label>
          {{$user->email}}
    </div>
    <div class="col-md-4 mb-1">
          <label for="validationDefault01">Celular:  </label>
          {{$user->mobile}}
    </div>
    <div class="col-md-4 mb-1">
          <label for="validationDefault01">Ciudad:  </label>
          {{$user->location}}
    </div>
    <div class="col-md-4 mb-1">
          <label for="validationDefault01">Dirección:  </label>
          {{$user->address}}
    </div>

 
@endsection
