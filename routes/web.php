<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'SearchController@searchWelcome' );
	

Route::post('login','Auth\LoginController@login')->name('login');
Route::post('logout','Auth\LoginController@logout')->name('logout');
Route::get('/registro', function () {
    return view('profiles.registry');
});

Route::get('/reg', function () {
    return view('users.registro');
});

Route::get('/edad', 'EdadController@edad' );

Route::resource('users', 'UserController');
Route::resource('profiles', 'ProfileController');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');



Route::get('/nosotros', 'SearchController@searchNosotros' );
Route::get('/planes', 'SearchController@searchPlanes' );
Route::get('/milof', 'SearchController@searchMilof' );



Route::get('/calificar', function () {
   return view('qualify');

});


Route::get('/politicas', function () {
   return view('policies.policies');

});
Route::get('/cursos', function () {
   return view('courses.courses');

});


